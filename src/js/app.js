"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();
